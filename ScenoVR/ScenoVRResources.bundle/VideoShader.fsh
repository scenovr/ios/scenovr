/**
 *
 * Copyright (c) 2016 xzimg , All Rights Reserved
 * No part of this software and related documentation may be used, copied,
 * modified, distributed and transmitted, in any form or by any means,
 * without the prior written permission of xzimg
 *
 * the xzimg company is registered at 903 Dannies House, 20 Luard Road, Wanchai, Hong Kong
 * contact@xzimg.com, xzimg.com
 *
 */
precision mediump float;
varying lowp vec2 fragTexCoord;

uniform sampler2D textureSamplerY;
uniform sampler2D textureSamplerUV;

void main()
{
    float y = texture2D(textureSamplerY, fragTexCoord).r;
    float u = texture2D(textureSamplerUV, fragTexCoord).g - 0.5;
    float v = texture2D(textureSamplerUV, fragTexCoord).r - 0.5;
    gl_FragColor = vec4(y + 1.73*u,  y - 0.34*u - 0.70*v, y + 1.37*v, 1.0);
}
