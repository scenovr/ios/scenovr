attribute vec4 Position;

uniform mat4 modelViewProjection;
attribute vec2 TexCoordIn; // New
varying vec2 fragTexCoord; // New

void main(void) {
    gl_Position = modelViewProjection * Position;
    fragTexCoord = TexCoordIn; // New
}
