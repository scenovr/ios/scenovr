precision mediump float;

uniform float rotation;
uniform vec2 scale;

mat4 modelViewMatrix = u_modelViewTransform;
mat4 projectionMatrix = u_projectionTransform;

vec2 position = _geometry.position.xy;

vec2 alignedPosition = position * scale;

vec2 rotatedPosition;
rotatedPosition.x = cos( rotation ) * alignedPosition.x - sin( rotation ) * alignedPosition.y;
rotatedPosition.y = sin( rotation ) * alignedPosition.x + cos( rotation ) * alignedPosition.y;

vec4 finalPosition;

finalPosition = modelViewMatrix * vec4( 0.0, 0.0, 0.0, 1.0 );
finalPosition.xy += rotatedPosition;
finalPosition = projectionMatrix * finalPosition;

_geometry.position = u_inverseModelViewProjectionTransform * finalPosition;

