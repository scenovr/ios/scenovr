//
//  ScenoVR.h
//  ScenoVR
//
//  Created by Jerome Boisson on 2/23/16.
//  Copyright © 2016 Gimbal Cube. All rights reserved.
//

/**** 
 JB : 
 REMOVED DEBUG SYMBOLS :
 
 ' Strip Debug Symbols During Copy ' to YES
 ' Debug Information Format ' to 'DWARF with dSYM File'
 ' Generate Debug Symbols ' to 'NO'
 ' Symbols Hidden by Default ' to 'YES'
*****/

#import <Foundation/Foundation.h>
//! Project version number for Product360.
FOUNDATION_EXPORT double Product360VersionNumber;

//! Project version string for Product360.
FOUNDATION_EXPORT const unsigned char Product360VersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Product360/PublicHeader.h>

#import "GcTourViewController.h"
#import "GcToursManager.h"
#import "GcTour.h"
#import "GcTours.h"
#import "GcPanorama.h"
#import "GcPanoramaPhoto.h"
#import "GcPanoramaVideo.h"
#import "GcPanorama.h"
#import "GcPointOfInterest.h"
#import "GcTexture.h"
#import "GcTextureThumbnail.h"
#import "GcPanoramaPOIInfo.h"
#import "GcMetadata.h"

@interface ScenoVR : NSObject

@end
