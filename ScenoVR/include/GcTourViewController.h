//
//  GcTourViewController.h
//  Product360
//
//  Created by Yann Miecielica on 04/09/2015.
//  Copyright (c) 2015 gimbalcube. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>
#import <SceneKit/SceneKit.h>
#import <QuartzCore/QuartzCore.h>
#import "GcPointOfInterestCustom.h"
#import "GcPointOfInterestPanorama.h"

typedef enum : NSInteger {
    Product360LoadErrorDownloadJson = 0,
    Product360LoadErrorBadJsonFile,
    Product360LoadErrorDownloadTextureSD,
    Product360LoadErrorDownloadTextureHD,
    Product360LoadErrorDownloadTextureThumbnail,
    Product360LoadErrorFailToWriteTextureOnDisk,
    Product360LoadErrorPanoramaDoesNotBelongToTour,
    Product360LoadErrorNotAllowed,
    Product360LoadErrorBadMetadataFilter,
    Product360LoadErrorNoNext
} Product360LoadError;

typedef enum : NSInteger {
    Product360DeviceErrorCardboardNotAvailable = 0
} Product360DeviceError;

typedef enum : NSInteger {
    Product360RotationModeWithDeviceMotion = (1 << 0), // => 00000001
    Product360RotationModeWithTouch        = (1 << 1) // => 00000010
} Product360RotationMode;

typedef enum : NSInteger {
    Product360RenderModeMonoscopic = 0,
    Product360RenderModeStereoscopic
} Product360RenderMode;

typedef enum : NSUInteger {
    Product360NavigationModeTouchROI = (1 << 0), // => 00000001
    Product360NavigationModeAimROI   = (1 << 1) // => 00000010
} Product360NavigationMode;

@class GcTour, GcTours, GcPanorama;

/*!
 @protocol Product360Delegate
 
 @discussion
 The Product360Delegate is intended to be used in conjunction with the Product360ViewController.
 These required delegate methods will be triggered after calling the load method loadPanoTourFromURL.
 */
@protocol GcTourViewControllerDelegate <NSObject>
@required
-(void)gcTourViewController:(id)tourViewController panoramaWillLoad:(GcPanorama*)panorama;
-(void)gcTourViewController:(id)tourViewController panoramaDidDisplay:(GcPanorama*)panorama inHD:(BOOL)isHD;
-(void)gcTourViewController:(id)tourViewController panoramaFailedToLoad:(GcPanorama*)panorama withError:(Product360LoadError)loadError;
-(void)gcTourViewController:(id)tourViewController modeFailedToLoadOnDevice:(Product360DeviceError)deviceError;
-(void)onFloorButtonPushedInTourViewController:(id)tourViewController;
-(void)onReplayButtonPushedInTourViewController:(id)tourViewController;

-(void)gcTourViewController:(id)tourViewController videoPanoramaDidLoad:(GcPanorama*)panorama withDuration:(Float64)duration;
-(void)gcTourViewController:(id)tourViewController videoPanoramaDidPause:(GcPanorama*)panorama;
-(void)gcTourViewController:(id)tourViewController videoPanoramaDidStart:(GcPanorama*)panorama;
-(void)gcTourViewController:(id)tourViewController videoPanoramaDidStop:(GcPanorama*)panorama;
-(void)gcTourViewController:(id)tourViewController videoPanoramaUpdated:(GcPanorama*)panorama isBuffering:(BOOL)isBuffering;
-(void)gcTourViewController:(id)tourViewController videoPanoramaUpdated:(GcPanorama*)panorama withElapsedTime:(int)time andTotalTime:(int)totalTime;
-(void)gcTourViewController:(id)tourViewController videoPanoramaUpdated:(GcPanorama*)panorama withBufferingPercentage:(int)percent;

-(void)gcTourViewController:(id)tourViewController customPOITriggered:(GcPointOfInterestCustom*)poi;
-(void)gcTourViewController:(id)tourViewController customPopupButtonTriggered:(NSInteger)popupButtonTag poi:(GcPointOfInterest *)poi;
-(GcPanoramaPOIInfo*)gcTourViewController:(id)tourViewController panoramaPOIInfoForPOI:(GcPointOfInterestPanorama*)panoramaPOI;
-(GCCustomPOIInfo*)gcTourViewController:(id)tourViewController customPOIInfoForPOI:(GcPointOfInterestCustom*)customPOI;

@end

/*!
 @interface GcTourViewController
 
 @discussion
 Use the GcTourViewController as a root class for your own view controller
 in order to use it to load throught it a set of 360 images from a distant server
 and then display them with different options.
 
 
 Example usage:
 @code
 
 @interface myTourViewController : GcTourViewController
 @end
 
 @implementation myTourViewController
 @end
 
 myTourViewController* controller = [[myTourViewController alloc] initWithNibName:@"myTourViewController" bundle:nil];
 [controller lloadToursFromURL:@"http://projects.gimbalcube.com/Ecocea/AccorPanoramaRepository/tours.json"
            andCompletionBlock:^(GcTours *tours)
            {
                if (tours == nil)
                    NSLog(@"issues loading tour");
                else{
                        GcTour* tour = [tours getTourWithID:@"68818c7e-c962-4a2a-9517-ecc0d1085e56"];
                        [controller displayPanorama:[tour getPanoramaWithDisplayOrder:0]];
                    }
            }];
[self.navigationController pushViewController:controller animated:NO];
 
 @endcode
 */
@interface GcTourViewController : UIViewController


- (instancetype)initWithTour:(GcTour*)tour;

/*!
 Display in the view the first panorama object in tour.
 
 Example usage:
 @code
 [product360ViewController displayFirstPanoramaInTour];
 @endcode
 @param panorama The tour which contains the panoama to display.
 */
- (void)displayPanorama:(GcPanorama*)panorama;

/*!
 Set the Navigation mode used to navigate from a poi to another (e.g: aim the poi or touch the poi)
 N.B: Only the aim mode is used when Stereoscopic mod is enable.
 
 Example usage:
 @code
 [product360ViewController setNavigationMode:Product360NavigationModeTouchROI];
 @endcode
 @param navigationMode The type of navigation.
 */
- (void)setNavigationMode:(Product360NavigationMode)navigationMode;

/*!
 Set the type of render (e.g: Stereoscopic or Monoscopic render)
 
 Example usage:
 @code
 [product360ViewController setRenderMode:Product360RenderModeStereoscopic];
 @endcode
 @param renderMode The type of render.
 */
- (void)setRenderMode:(Product360RenderMode)renderMode;

/*!
 Set the type information use to rotate the camera (e.g: Gyro or Touch)
 
 Example usage:
 @code
 [product360ViewController setRotationMode:Product360RotationModeWithDeviceMotion];
 @endcode
 @param rotationMode The type of rotation.
 */
- (void)setRotationMode:(Product360RotationMode)rotationMode;


- (void)setPointOfInterestActivatedImage:(UIImage*)image __attribute__((deprecated(("setPointOfInterestActivatedImage has been deprecated please use setPhotoPOISelectedOverlay and setVideoPOISelectedOverlay instead"))));

- (void)setPhotoPOISelectedOverlay:(UIImage*)image;
- (void)setVideoPOISelectedOverlay:(UIImage*)image;
- (void)setVideoPanoramaOverlayColor:(UIColor*)color;

- (void)setFuseButtonImages:(NSArray*)images __attribute__((deprecated(("setFuseButtonImages has been deprecated please use initFuseButton instead"))));
- (void)setFuseButtonAnimationDuration:(float)animationDuration __attribute__((deprecated(("setFuseButtonAnimationDuration has been deprecated please use initFuseButton instead"))));

- (void)initFuseButton:(NSArray*)images withDuration:(float)animationDuration;

- (void)setFuseButtonHidden:(BOOL)hidden;
- (BOOL)isFuseButtonHidden;

- (void)setFloorButtonImage:(UIImage*)image
                   forState:(UIControlState)state;
- (void)setFloorButtonEnabled:(BOOL)enabled;
- (void)setFloorButtonSelected:(BOOL)selected;
- (void)setFloorButtonHidden:(BOOL)hidden;
- (BOOL)isFloorButtonEnabled;
- (BOOL)isFloorButtonSelected;
- (BOOL)isFloorButtonHidden;

- (void)setTimeoutIntervalForRequest:(float)timeoutIntervalForRequest;
- (void)setTimeoutIntervalForResource:(float)timeoutIntervalForResource;

- (void)setReplayButtonImage:(UIImage*)image
                   forState:(UIControlState)state;
- (void)setReplayButtonEnabled:(BOOL)enabled;
- (void)setReplayButtonSelected:(BOOL)selected;
- (BOOL)isReplayButtonEnabled;
- (BOOL)isReplayButtonSelected;

- (void)playCurrentVideoPanorama;
- (void)pauseCurrentVideoPanorama;
- (void)resumeCurrentVideoPanorama;
- (void)seekToCurrentVideoPanorama:(float)timeSeconds;
- (void)seekFromPosition:(float)timeSeconds;
- (void)replayVideoPanorama;
- (float)getVideoDuration;

@property (nonatomic, strong) GcTour* tour;

/*!
 @property source
 Set source to the GcTourViewControllerDelegate.
 This is typically the View Controller based on GcTourViewController.
 */
@property (nonatomic, weak) id <GcTourViewControllerDelegate> delegate;

/*!
 Load all textures of a panorama and cache them to disk for an offline use
 
 @param panorama The panorama to cache.
 */
- (void)cachePanorama:(GcPanorama *)panorama
			  success:(void (^)(GcPanorama *panorama, bool isFullyHDLoaded))successHandler
			  failure:(void (^)(Product360LoadError loadError, NSError *error))failureHandler;

@end
