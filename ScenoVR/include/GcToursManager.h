//
//  GcToursManager.h
//  Product360
//
//  Created by Yann Miecielica on 14/02/2016.
//  Copyright © 2016 Gimbal Cube. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ScenoVR.h"
#import "GcMetadata.h"

@class GcTours, GcPanorama;

typedef enum {
	STAGING,
	PRODUCTION
} ENVIRONMENT;

@interface GcToursManager : NSObject

@property (readonly, nonatomic, strong) GcTours *tours;
@property (nonatomic, assign) unsigned long long int cacheSize;

+ (GcToursManager*)defaultManager;

#pragma mark - API KEY -

/*!
 Set the environment
 
 Example usage:
 @code
 [[GcToursManager defaultManager] setEnvironment:ENVIRONMENT];
 @endcode
 @param environment The environment
 */
- (void)setEnvironment:(ENVIRONMENT)environment;

/*!
 Set the api key used to retrieve data
 
 Example usage:
 @code
 [[GcToursManager defaultManager] setApiKey:@"API_KEY"];
 @endcode
 @param apiKey The API key
 */
- (void)setApiKey:(NSString *)apiKey;

#pragma mark - TOURS LOADING -

/*!
 Load paginated tours from server
 
 Example usage:
 @code
 [[GcToursManager defaultManager] loadToursWithCompletion:^(NSArray *tours, BOOL hasMore) {
 //code
 } failure:^(Product360LoadError error) {
 //code
 }];
 @endcode
 @note Block returns an error in case of failure
 @note Block tours return contains tours loaded from server
 @note Block hasMore indicate if there is more data to load from this point
 */
- (void)loadToursWithCompletion:(void (^)(NSArray *tours, BOOL hasMore))completion
						failure:(void (^)(Product360LoadError error))failure;

/*!
 Load tours from given url
 
 Example usage:
 @code
 [[GcToursManager defaultManager] loadToursFromURL:url  withHeaders:headers withCompletion:^(NSArray *tours, BOOL hasMore) {
 // code
 } failure:^(Product360LoadError error) {
 // code
 }];
 @endcode
 @param url The URL to load JSON
 @param headers An optional paramater to pass headers to request
 @note Block returns an error in case of failure
 @note Block tours return contains tours loaded from server
 @note Block hasMore indicate if there is more data to load from this point
 */
- (void)loadToursFromURL:(NSString*)url
			 withHeaders:(NSDictionary *)headers
		  withCompletion:(void (^)(NSArray *tours, BOOL hasMore))completion
				 failure:(void (^)(Product360LoadError error))failure;

/*!
 Load more paginated data from the server
 
 Example usage:
 @code
 [[GcToursManager defaultManager] loadMoreDataWithCompletion:^(NSArray *moreData, BOOL hasMore) {
 //code
 } failure:^(Product360LoadError error) {
 //code
 }];
 @endcode
 @note Block returns an error in case of failure
 @note Block moreData return contains data loaded from server
 @note Block hasMore indicate if there is more data to load from this point
 */
- (void)loadMoreDataWithCompletion:(void (^)(NSArray *moreData, BOOL hasMore))completion
						   failure:(void (^)(Product360LoadError error))failure;

/*!
 Load tour with local video
 @param panoramaName The name of the panorama to display
 @param localPath The path of the local video to load
 @param thumbnailUrl The URL of the thumbnail
 @note Block returns an error in case of failure
 @note Block tour return contains tour loaded
 */
- (void)loadStandAloneTourWithVideo:(NSString *)panoramaName
			 localPath:(NSString *)localPath
		  thumbnailUrl:(NSURL *)thumbnailUrl
		  withCompletion:(void (^)(GcTour *tour))completion
				 failure:(void (^)(Product360LoadError error))failure;

#pragma mark - FILTERING -

/*!
 Find tours from the server with parameters
 
 Example usage:
 @code
 [[GcToursManager defaultManager] findToursWithFilters:toursFilters
 completion:^(NSArray *filteredTours, BOOL hasMore) {
 //code
 } failure:^(Product360LoadError error) {
 //code
 }];
 @endcode
 @param filters A dictionary defining filters to apply
 @note Block returns an error in case of failure
 @note Block filteredTours return contains incremental tours loaded from server
 @note Block hasMore indicate if there is more data to load from this point
 */
- (void)findToursWithFilters:(NSDictionary *)filters
				  completion:(void (^)(NSArray *filteredTours, BOOL hasMore))completion
					 failure:(void (^)(Product360LoadError error))failure;

/*!
 Find panoramas from the server with parameters
 
 Example usage:
 @code
 [[GcToursManager defaultManager] findPanoramasWithFilters:toursFilters
 completion:^(NSArray *filteredTours, BOOL hasMore) {
 //code
 } failure:^(Product360LoadError error) {
 //code
 }];
 @endcode
 @param filters A dictionary defining filters to apply
 @note Block returns an error in case of failure
 @note Block filteredTours return contains only one tour containing all found panoramas
 @note Block hasMore indicate if there is more data to load from this point
 */
- (void)findPanoramasWithFilters:(NSDictionary *)filters
					  completion:(void (^)(NSArray *filteredTours, BOOL hasMore))completion
						 failure:(void (^)(Product360LoadError error))failure;

- (GcTour *)getTourWithID:(NSString*)tourID;
- (GcTour *)getTourWithDisplayOrder:(int)displayOrder;
- (NSArray *)getTours;

#pragma mark - PANORAMA LOADING

- (void)loadPanoramaAndPointsOfInterest:(GcPanorama*)panorama
							withSuccess:(void (^)(id panorama, bool isFullyHDLoaded))successHandler
								failure:(void (^)(id panorama, Product360LoadError loadError, NSError *error))failureHandler
							   POIBlock:(GcPOIInfo* (^)(GcPointOfInterest* poi))POIHandler
						   isForCaching:(BOOL)isForCaching;

#pragma mark - CACHE MANAGING

- (void)clearCacheIfNeeded;

- (void)clearCache;

/*!
 Cache the JSON tour to disk, then cache all panoramas textures to disk too.
 
 @param tour The entire tour to cache.
 */
- (void)cacheTour:(GcTour *)tour
		  success:(void (^)(GcTour *tour, bool isCacheCompleted))successHandler
		  failure:(void (^)(Product360LoadError loadError))failureHandler;
/*!
 Cache all tours to disk, then cache all panoramas textures to disk too.
 */
- (void)cacheAllToursWithSuccess:(void (^)(NSMutableArray *cachedTours, bool isCacheCompleted))successHandler
						 failure:(void (^)(Product360LoadError loadError))failureHandler;
/*!
 Return all Tour Ids from disk cache.
 */
- (NSArray *)getCachedToursIds;
/*!
 Load a given tour from cache
 
 @param tourId The Tour Id to load from disk cache
 */
- (NSDictionary *)loadCachedTour:(NSString *)tourId;
/*!
 Load all cached tours from disk
 */
- (void)loadCachedToursWithCompletion:(void (^)(NSArray *tours))completion;

@end
