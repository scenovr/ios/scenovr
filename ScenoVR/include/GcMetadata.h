//
//  GcMetadata.h
//  ScenoVR
//
//  Created by Aurélien WOLFERT on 18/01/2017.
//  Copyright © 2017 Gimbal Cube. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GcMetadata: NSObject

- (instancetype)initWithMetadata:(NSDictionary *)metadata;

@property (nonatomic, strong) NSDictionary *metadata;

@end
