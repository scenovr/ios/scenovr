//
//  GcPointOfInterestPanorama.h
//  ScenoVR
//
//  Created by Jerome Boisson on 8/11/16.
//  Copyright © 2016 Gimbal Cube. All rights reserved.
//

#import "GcPointOfInterest.h"
#import "GcPanoramaPOIInfo.h"

@interface GcPointOfInterestPanorama : GcPointOfInterest
{
    NSString* _panoramaId;
    GcPanoramaPOIInfo* _panoramaInfos;
    BOOL _panoramaInfoHasBeenSet;
}

@property (readonly, nonatomic, strong) NSString* panoramaId;
@property (nonatomic, strong) GcPanoramaPOIInfo* panoramaInfos;


@end
