//
//  GcPointOfInterestCustom.h
//  ScenoVR
//
//  Created by Jerome Boisson on 8/11/16.
//  Copyright © 2016 Gimbal Cube. All rights reserved.
//

#import "GcPointOfInterest.h"
#import "GcCustomPOIInfo.h"
#import "GcTexture.h"

@interface GcPointOfInterestCustom : GcPointOfInterest
{
    GCCustomPOIInfo* _customInfos;
    NSString* _name;
    NSString* _action;
    NSString* _value;
    GcTexture* _texture;
    BOOL _customInfoHasBeenSet;
}

@property (nonatomic, strong) GCCustomPOIInfo* customInfos;
@property (readonly, nonatomic, strong) NSString* name;;
@property (readonly, nonatomic, strong) NSString* action;
@property (readonly, nonatomic, strong) NSString* value;



@end
