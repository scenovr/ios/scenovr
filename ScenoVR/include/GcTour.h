//
//  GcTour.h
//  Product360
//
//  Created by Yann Miecielica on 2/11/16.
//  Copyright © 2016 Gimbal Cube. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GcObject.h"

@class GcTours, GcPanorama;

@interface GcTour : GcObject
{
    NSString* _tourID;
    NSString* _name;
    NSNumber* _displayOrder;
    GcTours* _superTours;
    NSDictionary* _panoramas;
}

@property (readonly, nonatomic, strong) NSString* tourID;
@property (readonly, nonatomic, strong) NSString* name;
@property (readonly, nonatomic, strong) NSNumber* displayOrder;
@property (readonly, nonatomic, strong) GcTours* superTours;

- (GcPanorama*)getPanoramaWithID:(NSString*)panoramaID;
- (GcPanorama*)getPanoramaWithDisplayOrder:(int)displayOrder;
- (NSArray*)getPanoramas;

@end
