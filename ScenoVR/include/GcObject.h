//
//  GcObject.h
//  ScenoVR
//
//  Created by Jerome Boisson on 9/20/16.
//  Copyright © 2016 Gimbal Cube. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GcObject : NSObject
{
    NSDictionary* _metadata;
}


@property (readonly, nonatomic, strong) NSDictionary* metadata;

@end
