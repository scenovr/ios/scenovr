//
//  GcPointOfInterest.h
//  Product360
//
//  Created by Yann Miecielica on 2/11/16.
//  Copyright © 2016 Gimbal Cube. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GcObject.h"
#import "GcPOIInfo.h"

@class GcPanorama;

@interface GcPointOfInterest : GcObject
{
    NSNumber* _x;
    NSNumber* _y;
    GcPanorama* _superPanorama;

}

@property (readonly, nonatomic, strong) NSNumber* x;
@property (readonly, nonatomic, strong) NSNumber* y;
@property (readonly, nonatomic, strong) GcPanorama* superPanorama;

- (void)setInfos:(GcPOIInfo *)infos;

@end
