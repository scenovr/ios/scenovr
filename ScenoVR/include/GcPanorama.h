//
//  GcPanorama.h
//  Product360
//
//  Created by Yann Miecielica on 2/11/16.
//  Copyright © 2016 Gimbal Cube. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GcObject.h"
#import <UIKit/UIKit.h>

@class GcTour, GcTextureThumbnail, GcPointOfInterest;

@interface GcPanorama : GcObject
{
    NSString* _panoramaID;
    NSString* _name;
    NSNumber* _displayOrder;
    GcTextureThumbnail* _thumbnail;
    NSArray* _pointsOfInterest;
    GcTour* _superTour;
	double _center;
}

@property (readonly, nonatomic, strong) NSString* panoramaID;
@property (readonly, nonatomic, strong) NSString* name;
@property (readonly, nonatomic, strong) NSNumber* displayOrder;
@property (readonly, nonatomic, strong) GcTextureThumbnail* thumbnail;
@property (readonly, nonatomic, strong) NSArray* pointsOfInterest;
@property (readonly, nonatomic, strong) GcTour* superTour;
@property (readonly, nonatomic) double center;

- (GcPointOfInterest*)getPointOfInterestWithID:(NSString*)pointOfInterestID;

- (BOOL)isLoaded;
- (BOOL)isFullyHDLoaded;
- (BOOL)isThumbnailLoaded;
- (BOOL)isVideo;

- (void)getThumbnailImageWithSuccess:(void (^)(UIImage *thumbnail, bool isFound))successHandler;

@end
