//
//  GcTextureThumbnail.h
//  Product360
//
//  Created by Yann Miecielica on 2/11/16.
//  Copyright © 2016 Gimbal Cube. All rights reserved.
//

#import "GcTexture.h"

@interface GcTextureThumbnail : GcTexture

@end