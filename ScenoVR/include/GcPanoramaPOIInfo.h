//
//  GcPanoramaPOIInfo.h
//  ScenoVR
//
//  Created by Aurélien WOLFERT on 22/12/2016.
//  Copyright © 2016 Gimbal Cube. All rights reserved.
//

#import "GcPOIInfo.h"

@interface GcPanoramaPOIInfo : GcPOIInfo {
    BOOL _panoramaRounded;
}

@property (readonly, nonatomic, strong) UIImage* poiURLImage;

- (instancetype)initWithPOISize:(CGSize)poiSize;
- (void)setPOIImage:(UIImage *)poiImage withRound:(BOOL)rounded;
- (void)setPOIImageURL:(NSString *)poiImageURL withRound:(BOOL)rounded;
- (BOOL)getPanoramaRounded;

@end
