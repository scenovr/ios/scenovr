//
//  GcTours.h
//  Product360
//
//  Created by Yann Miecielica on 2/11/16.
//  Copyright © 2016 Gimbal Cube. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GcTour;

@interface GcTours : NSObject {
    NSDictionary *_tours;
    BOOL _isVirtual;
}

#pragma mark - TOURS -
	
@property (nonatomic, strong) NSDictionary *tours;

- (GcTour *)getTourWithID:(NSString*)tourID;
- (GcTour *)getTourWithDisplayOrder:(int)displayOrder;
- (NSArray *)getTours;

@end
