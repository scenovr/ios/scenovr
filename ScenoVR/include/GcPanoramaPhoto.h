//
//  GcPanoramaPhoto.h
//  Product360
//
//  Created by Yann Miecielica on 2/11/16.
//  Copyright © 2016 Gimbal Cube. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GcPanorama.h"

@interface GcPanoramaPhoto : GcPanorama
{
    NSMutableArray* _texturesSD;
    NSMutableArray* _texturesHD;
}

@property (readonly, nonatomic, strong) NSMutableArray* texturesSD;
@property (readonly, nonatomic, strong) NSMutableArray* texturesHD;

- (BOOL)isLoaded;
- (BOOL)isFullyHDLoaded;

@end
