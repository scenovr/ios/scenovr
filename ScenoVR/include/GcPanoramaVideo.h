//
//  GcPanoramaVideo.h
//  Product360
//
//  Created by Yann Miecielica on 2/11/16.
//  Copyright © 2016 Gimbal Cube. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GcPanorama.h"

typedef enum : NSInteger {
    ScenoVRVideoProjectionTypeEquirectangular = 0,
    ScenoVRVideoProjectionTypePlaneCubemap32,
    ScenoVRVideoProjectionTypeCubemap180,
    ScenoVRVideoProjectionTypeCubemapInv
} ScenoVRVideoProjectionType;

@interface GcPanoramaVideo : GcPanorama
{
    NSString* _videoUrl;
    NSNumber* _viewAngle;
    NSNumber* _isStereo;
    ScenoVRVideoProjectionType _projectionType;
}

@property (readonly, nonatomic, strong) NSString* videoUrl;
@property (readonly, nonatomic, strong) NSNumber* viewAngle; // Int
@property (readonly, nonatomic, strong) NSNumber* isStereo;  // Boolean
@property (readonly, nonatomic, assign) ScenoVRVideoProjectionType projectionType;

@end
