//
//  GcTexture.h
//  Product360
//
//  Created by Yann Miecielica on 2/11/16.
//  Copyright © 2016 Gimbal Cube. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class GcPanorama;

typedef enum : NSInteger {
    GcTextureIndexes01 = 0,
    GcTextureIndexes02,
    GcTextureIndexes03,
    GcTextureIndexes04,
} GcTextureIndexes;

@interface GcTexture : NSObject
{
    NSString* _url;
    NSString* _path;
    NSString* _fileName;
    GcTextureIndexes _index;
    GcPanorama* _superPanorama;
    float _priority;
    BOOL _isLoading;

}

@property (readonly, nonatomic, strong) NSString* url;
@property (readonly, nonatomic, strong) NSString* path;
@property (readonly, nonatomic, strong) NSString* fileName;
@property (readonly, nonatomic, assign) GcTextureIndexes index;
@property (readonly, nonatomic, strong) GcPanorama* superPanorama;
@property (readonly, nonatomic, assign) float priority;
@property (readonly, nonatomic, assign) BOOL isLoading;

- (UIImage*)getTextureImage;

@end
