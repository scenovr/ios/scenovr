//
//  GCCustomPOIInfo.h
//  ScenoVR
//
//  Created by Jerome Boisson on 8/10/16.
//  Copyright © 2016 Gimbal Cube. All rights reserved.
//

#import "GcPOIInfo.h"

@interface GCCustomPOIInfo : GcPOIInfo {
    UIImage* _overlay;
    BOOL _customRounded;
    BOOL _visible;
}

@property (readonly, nonatomic, strong) UIImage* overlay;

- (instancetype)initWithURL:(NSString *)url andOverlay:(UIImage*)overlay withVisibility:(BOOL)visible andRound:(BOOL)rounded andPOISize:(CGSize)poiSize;
- (instancetype)initWithImage:(UIImage *)poiImage andOverlay:(UIImage*)overlay withVisibility:(BOOL)visible andRound:(BOOL)rounded andPOISize:(CGSize)poiSize;
- (BOOL)getCustomRounded;
- (BOOL)getVisible;

@end
