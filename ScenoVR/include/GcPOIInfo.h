//
//  GcPOIInfo.h
//  ScenoVR
//
//  Created by Aurélien WOLFERT on 22/12/2016.
//  Copyright © 2016 Gimbal Cube. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, AnimationType) {
    BounceAndScale,
    FadeInAndOut
};

@interface GcPOIInfo : NSObject {
    UIImage* _poiImage;
    CGSize _poiSize;
    AnimationType _animationType;
    CGFloat _animationDuration;
    UIImage *_popupToDisplay;
    CGSize _popupSize;
    NSArray <NSString *>* buttonHotsPointsPositions;
    NSArray <NSNumber *>* buttonHotsPointsTags;
    NSString* _url;
}

@property (readonly, nonatomic, strong) UIImage* poiImage;
@property (nonatomic) CGSize poiSize;
@property (readonly, nonatomic) AnimationType animationType;
@property (readonly, nonatomic) CGFloat animationDuration;
@property (readonly, nonatomic, strong) UIImage* popupToDisplay;
@property (readonly, nonatomic) CGSize popupSize;
@property (readonly, nonatomic, strong) NSArray <NSString *>* buttonHotsPointsPositions;
@property (readonly, nonatomic, strong) NSArray <NSNumber *>* buttonHotsPointsTags;
@property (readonly, nonatomic, strong) NSString* url;

- (void)setAnimationType:(AnimationType)animationType withDuration:(CGFloat)animationDuration;
- (void)setPopupToDisplay:(UIView *)popupToDisplay withPopupSize:(CGSize)popupSize;
- (void)setPopupToDisplay:(UIView *)popupToDisplay withPopupSize:(CGSize)popupSize withButtonsHotPointsPositions:(NSArray <NSString *>*)buttonsHotsPointsPositions forTags:(NSArray <NSNumber *>*)tags;

@end
