# Change Log
All notable changes to this project will be documented in this file.

## [0.7.9] 2018-01-11
### Changed
Changed MotionManager queue

## [0.7.8] 2017-04-28
### Added
### Changed
Fix device rotation bug when Product360RotationModeWithTouch rotation mode is active
### Deprecated 
### Removed
### Fixed
### Security

## [0.7.7] 2017-04-25
### Added
'GcTourViewController' class implementation can now choose orientations via 'supportedInterfaceOrientations' method
### Changed  
### Deprecated 
### Removed
### Fixed
### Security

## [0.7.6] 2017-04-19
### Added
### Changed
Fix orientiation bug when push back to panorama view  

### Deprecated 
### Removed
### Fixed
### Security

## [0.7.5] 2017-04-03
### Added
### Changed
- `GCToursManager`
	- `- (void)loadToursFromURL:(NSString*)url withHeaders:(NSDictionary *)headers withCompletion:(void (^)(NSArray *tours, BOOL hasMore))completion failure:(void (^)(Product360LoadError error))failure` 

Availability to add optional headers

### Deprecated 
### Removed
### Fixed
### Security

## [0.7.4] 2017-03-28
### Added
- `GCToursManager`
	- `(void)setEnvironment:(ENVIRONMENT)environment` 
	- `(NSArray *)getTours`

### Changed
### Deprecated 
### Removed
### Fixed
### Security

## [0.7.3] 2017-03-26
### Added
- `GCToursManager`
	- `(GcTour *)getTourWithID:(NSString*)tourID`
	- `(GcTour *)getTourWithDisplayOrder:(int)displayOrder`

These methods can now be used whith GcToursManager, instead of using GcTours directly

### Changed
### Deprecated 
### Removed
### Fixed
### Security

## [0.7.2] 2017-03-14
### Added
### Changed
Cache managing improvement

### Deprecated 
### Removed
### Fixed
### Security

## [0.7.1] 2017-03-01
### Added
- `GCToursManager`
	- `(void)loadToursWithCompletion:(void (^)(NSArray *tours, BOOL hasMore))completion failure:(void (^)(Product360LoadError error))failure`
	- `(void)loadMoreDataWithCompletion:(void (^)(NSArray *moreData, BOOL hasMore))completion failure:(void (^)(Product360LoadError error))failure`

### Changed
- `GCToursManager`
	- `(void)findToursWithFilters:(NSDictionary *)filters completion:(void (^)(NSArray *filteredTours, BOOL hasMore))completion failure:(void (^)(Product360LoadError error))failure`
	- `(void)findPanoramasWithFilters:(NSDictionary *)filters completion:(void (^)(NSArray *filteredTours, BOOL hasMore))completion failure:(void (^)(Product360LoadError error))failure`

	`loadToursWithCompletion` and `loadMoreDataWithCompletion` now return tours array instead of GcTours object

### Deprecated 
### Removed
### Fixed
### Security

## [0.7.0] 2017-02-14
### Added
- `GCToursManager`
  - `(void)findTours:(NSDictionary *)metadata completionBlock:(void (^)(GcTours *))completionBlock failureBlock:(void (^)(Product360LoadError filterError, NSError *error))failureBlock;`
  - `(void)findPanoramas:(NSDictionary *)metadata completionBlock:(void (^)(GcTours *))completionBlock failureBlock:(void (^)(Product360LoadError filterError, NSError *error))failureBlock`  

### Changed
- `GCToursManager`
  - `setAppID` has been renamed to `setApiKey`
  
### Deprecated 
### Removed
### Fixed
### Security
