Pod::Spec.new do |s|

# TECHNICAL SPECIFICATIONS
  s.platform              = :ios
  s.ios.deployment_target = '8.0'
  s.requires_arc          = true

# VERSION
  s.version               = '0.8.4'

# NAME & DESCRIPTION
  s.name                  = 'ScenoVR'
  s.summary               = 'ScenoVR vous propose le meilleur de la visite virtuelle immersive. Explorez les lieux comme si vous y étiez !'

# LONG DESCRIPTION
  s.description           = <<-DESC
ScenoVR vous propose 3 modes de visites virtuelles en photo et/ou vidéo :
- En glissant votre doigt sur l'écran
- En bougeant votre smartphone ou tablette dans le sens souhaité
- En utilisant un dispositif de vue en réalité virtuelle comme les Google Cardboard.
                          DESC

# LICENSE
  s.license               = { :type => 'The ScenoVR SDK is free for software applications.', :file => 'LICENSE' }

# AUTHOR
  s.author                = { 'GimbalCube' => 'fabien@gimbalcube.com' }

# GITHUB HOMEPAGE
  s.homepage              = 'git@gitlab.com:scenovr/ios/scenovr.git'

# GITHUB SOURCE
  s.source                = { :git => 'git@gitlab.com:scenovr/ios/scenovr.git', :tag => s.version.to_s }

# FRAMEWORKS
#s.framework             = 'UIKit', 'Foundation'
s.weak_framework        = 'SceneKit', 'GLKit', 'QuartzCore', 'OpenGLES', 'SpriteKit', 'CoreMedia', 'AVFoundation', 'CoreMotion'

# SOURCE FILES
  s.source_files          = 'ScenoVR/include/*.{h}'

# RESOURCES
  s.resources             = 'ScenoVR/*.{bundle}'

# LIBRARIE
  s.vendored_libraries    = 'ScenoVR/libScenoVR.a'

end
