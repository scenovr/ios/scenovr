![N|Solid](https://pbs.twimg.com/profile_images/3365927952/6be52dab67f61ecd1c9bbc2a229ed357_200x200.png)

### Description

ScenoVR vous propose le meilleur de la visite virtuelle immersive. Explorez les lieux comme si vous y étiez !
ScenoVR vous propose 3 modes de visites virtuelles en photo et/ou vidéo :
- En glissant votre doigt sur l'écran
- En bougeant votre smartphone ou tablette dans le sens souhaité
- En utilisant un dispositif de vue en réalité virtuelle comme les Google Cardboard.

### Pré-requis

- Afin d’autoriser le projet à exécuter des requêtes http, on doit le définir dans le fichier info.plist du projet. Pour cela, on ajoute le dictionnaire ayant comme clé `App Transport Security Settings`, puis on définit la valeur `Allow Arbitrary Loads` à `YES`.

### Installation via CocoaPods

Ajouter simplement 'ScenoVR' à votre PODFILE comme ceci :

```sh
target 'YourProjectName' do
pod "ScenoVR"
end
```

Importer ensuite le framework ScenoVR :
```sh
#import <ScenoVR/ScenoVR.h>
```

### Installation manuelle

- Une fois le nouveau projet créé, il faut importer les fichiers de la librairie dans le projet.
Pour cela, il faut ajouter tous les .h présents dans le dossier ScenoVR/include. Penser à les ajouter à la target.
- Il faut également ajouter la librairie libScenoVR.a et le bundle ScenoVRResources. Penser à les ajouter à la target.
- Dans les Build Settings de la target, il faut ajouter le flag `-ObjC` pour la clé `Other Linker Flags`.
- Enfin, ajouter les frameworks suivants : `AVFoundation`, `CoreMedia`, `CoreMotion`

### Utilisation
Il faut ensuite créer à minima deux contrôleurs. 
Le premier sera en charge de charger et afficher la liste des tours. Le second permettra d’afficher et de prendre en charge la gestion d’un panorama.

Dans le premier contrôleur (type UIVIewController), le chargement des tours se fait de cette manière :
On fait appel au singleton [GCToursManager defaultManager] puis on appel successivement les méthodes suivantes à partir du singleton :
```sh
(void)setCacheSize:(int)cacheSize
(void)setApiKey:(NSString *)apiKey
(void)setEnvironment:(ENVIRONMENT)environment
(void)loadToursWithCompletion:(void (^)(NSArray *))completion failure:(void (^)(Product360LoadError error))failure
```
Il est également possible de charger un fichier JSON depuis une URL : 
```sh
(void)loadToursFromURL:(NSString*)url withHeaders:(NSDictionary *)headers withCompletion:(void (^)(NSArray *tours, BOOL hasMore))completion failure:(void (^)(Product360LoadError error))failure
```

Le paramètre `headers`est optionnel et permet de passer des paramètres à la requête si nécessaire. 

Le cache size par défaut est exprimé en octets. On le définit à 10Mo, soit 10000000 d’octets.
L’ApiKey correspond à la clé d’en-tête utilisée lors de l’appel au web service permettant de retourner les tours correspondants d’une entité.
L'environment désigne l'environnement à utiliser lors des appels aux web services (STAGING ou PRODUCTION).

Le block nous retourne une erreur en cas de `failure`, ou un tableau de tours trié par ordre d’affichage en cas de `completion`.
On pourra ainsi récupérer un tour spécifique (GcTour) à un index donné du tableau.

Afin d’accéder aux panoramas d’un tour, on fait appel à la méthode `(NSArray*)getPanoramas` à partir d’un tour, qui retourne un tableau de panoramas triés par ordre d’affichage.
On pourra ainsi récupérer un panorama spécifique (GcPanorama) à un index donné du tableau.

L'image d'un panorama peut être retrouvée comme ceci :
```sh
[NSURL URLWithString:panorama.thumbnail.url] 
```

Pour afficher un panorama, il faut créer un contrôleur de type UIViewController héritant de GcTourViewController et de GcTourViewControllerDelegate et le définir comme étant ce delegué.

Il faut également déclarer un attribut panorama comme ceci dans le .h qui permettra de transmettre le panorama désiré du premier contrôleur à l’autre :
```sh
@property (nonatomic, strong) GcPanorama *panorama;
```

A partir du contrôleur appelant, il faut donc instancier ce second contrôleur qui sera en charge d'afficher le panorama sélectionné.
Il faudra alors lui définir les propriétés suivantes avant de l'afficher :
`setVideoPOISelectedOverlay` : définit la sur-image d'un POI vidéo
`setPhotoPOISelectedOverlay` : définit la sur-image d'un POI photo
`delegate` : définit le contrôleur delegate en charge de réceptionner les évènements
`panorama` : panorama à charger
`initFuseButton` : images affichées dans le bouton lors du chargement d'un POI ou d'une action sur un bouton
`setVideoPanoramaOverlayColor` : couleur en surimpression à la fin d'un panorama
`setFloorButtonImage` : image du bouton affiché au sol pour changer de mode de vue
`setReplayButtonImage (normal et selected)` : image pour le bouton de replay
`displayPanorama` : permet d'afficher le panorama dans la vue du contrôleur
`setRenderMode` : définit le mode de rendu par défaut
`presentViewController` : affiche le contrôleur

### Utilisation des filtres

Il est possible de filtrer des tours ou des panoramas. Dans un premier temps, il faut les définir en respectant certaines conventions de nommage. 
On peut ajouter autant de couple tag/values que l'on souhaite, et autant de values pour un tag unique.

Voici un exemple de filtres pour les tours : 
```
{
"metaData":[{
"tag": "brandCode",
"values": ["ibi","sof"]
},
{
"tag": "type",
"values": ["visits"]
}]
} 
```

Exemple de filtres pour les panoramas : 
```
{
"metaData":[{
"tag": "roomtype",
"values": ["terrace"]
}]
}
```

Pour filtrer les tours, on crée donc un dictionnaire comme illustré précedemment, puis on fait appel à cette méthode : 
```
(void)findToursWithFilters:(NSDictionary *)filters
completion:(void (^)(NSArray *filteredTours, BOOL hasMore))completion
failure:(void (^)(Product360LoadError error))failure
```

Cette méthode prend en paramètre le dictionnaire de filtres, et retourne un block `failure` en cas d'erreur, ou un block `completion` contenant un tableau des tours filtrés. Le booléen `hasMore` indique si d'autres tours filtrés sont disponibles pour cette recherche.

Pour filtrer les panoramas, on crée donc un dictionnaire comme illustré précedemment, puis on fait appel à cette méthode : 
```
(void)findPanoramasWithFilters:(NSDictionary *)filters
completion:(void (^)(NSArray *filteredTours, BOOL hasMore))completion
failure:(void (^)(Product360LoadError error))failure
```

Cette méthode prend en paramètre le dictionnaire de filtres, et retourne un block `failure` en cas d'erreur, ou un block `completion` avec un tableau comprenant un tour unique contenant les panoramas filtrés. Le booléen `hasMore` indique si d'autres panoramas filtrés sont disponibles pour cette recherche.

### Charger plus de contenus

Il est possible de devoir charger plus de contenus, comme des tours, des tours filtrés ou des panoramas filtrés.
On peut savoir si du contenu supplémentaire est disponible pour chacun de ces cas grâce au booléen `hasMore` comme vu ci-dessus.

Pour charger plus de contenu, on fait appel à cette méthode : 
```
(void)loadMoreDataWithCompletion:(void (^)(NSArray *moreData, BOOL hasMore))completion
failure:(void (^)(Product360LoadError error))failure
```

Cette méthode retourne un block `failure` en cas d'erreur, ou un block `completion` contenant un tableau de données et le même booléen `hasMore`.
Concernant les tours et la recherche de tours, les données sont retournées de manière incrémentale, c'est-à-dire que ce ne sont pas tous les résultats qui seront retournés, mais uniquement les nouveaux.
Concernant la recherche de panoramas, un seul tour est retourné à chaque fois et contient tous les panoramas désirés.


### Delegates
```sh
(void)gcTourViewController:(id)tourViewController panoramaWillLoad:(GcPanorama*)panorama 
```
Méthode appelée lorsque le panorama est en cours d’initialisation et que son chargement va commencer.

```sh
(void)gcTourViewController:(id)tourViewController panoramaFailedToLoad:(GcPanorama*)panorama withError:(Product360LoadError)loadError
```
Méthode appelée lorsqu’une erreur est survenue pendant le chargement du panorama. L’erreur retournée est de type Product360LoadError.

```sh
(void)gcTourViewController:(id)tourViewController panoramaDidDisplay:(GcPanorama*)panorama inHD:(BOOL)isHD
```
Méthode appelée lorsque l’initialisation et le chargement d’un panorama autre que type vidéo sont terminés et est prêt à être affiché.

```sh
(void)gcTourViewController:(id)tourViewController modeFailedToLoadOnDevice:(Product360DeviceError)deviceError
```
Méthode appelée lorsque le mode cardboard n’est supporté sur l’appareil.

```sh
(void)onFloorButtonPushedInTourViewController:(id)tourViewController
```
Méthode appelée lorsque le bouton au sol a été sélectionné. On fait appel ici à la méthode suivante :
`[self setRenderMode :]` pour changer le mode de rendu (360RenderModeMonoscopic ou 360RenderModeStereoscopic).

```sh
(void)onReplayButtonPushedInTourViewController:(id)tourViewController
```
Méthode appelée lorsque le bouton de replay a été sélectionné. On fait appel ici à la méthode suivante :
`[self playCurrentVideoPanorama]` pour relancer la lecture du panorama.

```sh
(void)gcTourViewController:(id)tourViewController videoPanoramaDidLoad:(GcPanorama*)panorama withDuration:(Float64)duration
```
Méthode appelée lorsque le panorama a été complètement chargé. On fait appel ici aux méthodes suivantes :
`[self seekToCurrentVideoPanorama :0.0]` pour aller à un temps donné dans la vidéo
`[self playCurrentVideoPanorama]` pour commencer la lecture du panorama

```sh
(void)gcTourViewController:(id)tourViewController videoPanoramaDidPause:(GcPanorama*)panorama
```
Méthode appelée lorsque le panorama a été mis en pause

```sh
(void)gcTourViewController:(id)tourViewController videoPanoramaDidStart:(GcPanorama*)panorama
```
Méthode appelée lorsque la lecture du panorama a commencé

```sh
(void)gcTourViewController:(id)tourViewController videoPanoramaDidStop:(GcPanorama*)panorama
```
Méthode appelée lorsque la lecture du panorama a été arrêtée

```sh
(void)gcTourViewController:(id)tourViewController videoPanoramaUpdated:(GcPanorama*)panorama isBuffering:(BOOL)isBuffering
```
Méthode appelée à chaque changement d’état du panorama. On fait appel à la méthode suivante :
`[self pauseCurrentVideoPanorama]` si le panorama est en cours de chargement (isBuffering).
Sinon, on fait appeler à la méthode suivante :
`[self playCurrentVideoPanorama]` pour relancer la lecture du panorama.

```sh
(void)gcTourViewController:(id)tourViewController videoPanoramaUpdated:(GcPanorama*)panorama withElapsedTime:(int)time andTotalTime:(int)totalTime
```
Méthode appelée lorsque le panorama est en lecture et indique le temps de lecture actuel.

```sh
(void)gcTourViewController:(id)tourViewController videoPanoramaUpdated:(GcPanorama*)panorama withBufferingPercentage:(int)percent
```
Méthode appelée lorsque le panorama est chargée et mis en mémoire tampon. Idéal pour afficher un pourcentage de chargement.

```sh
(void)gcTourViewController:(id)tourViewController customPOITriggered:(GcPointOfInterestCustom*)poi
```
Méthode appelée lorsqu’un POI a été sélectionné sur un panorama. On peut lancer par exemple son URL associée.

```sh
(GcPanoramaPOIInfo *)gcTourViewController:(id)tourViewController panoramaPOIInfoForPOI:(GcPointOfInterestPanorama *)panoramaPOI
```
Méthode permettant de construire un POI de type panorama. Pour cela, on l’initialise avec une taille : 
`[[GcPanoramaPOIInfo alloc] initWithPOISize:CGSizeMake(1.4, 1.4)];`

Lors du survol d’un POI, on peut également personnaliser son animation. Pour cela, on fera appel à cette méthode : 
`- (void)setAnimationType:(AnimationType)animationType withDuration:(CGFloat)animationDuration`
`BounceAndScale` et `FadeInAndOut` sont les deux types d’animations possibles.

Il est possible d'ajouter une image personnalisée à un POI panorama (importée du projet ou à partir d'une URL :
`- (void)setPOIImage:(UIImage *)poiImage withRound:(BOOL)rounded`
`- (void)setPOIImageURL:(NSString *)poiImageURL withRound:(BOOL)rounded`

Enfin, la popup peut-être rendue dynamique en y ajoutant des boutons dans le XIB. Ceux-ci seront retransmis en modèle 3D afin d’être définis en tant que hotpoints.
Pour définir un hot point, on ajoute au XIB un bouton et on repère ses coordonnées. On crée ensuite une chaîne de caractères définissant ses coordonnées, et on lui attribue un tag :
`NSString *buttonHotPointPosition1 = NSStringFromCGRect(CGRectMake(217.0, 0.0, 83.0, 109.0));`
`NSNumber *buttonHotPointTag1 = [NSNumber numberWithInt:3];`

Le tag permettra de définir le bouton pour exécuter une méthode particulière.

Pour ajouter ces hotpoints à la popup, on fait appel à cette méthode : 
`- (void)setPopupToDisplay:(UIView *)popupToDisplay withPopupSize:(CGSize)popupSize withButtonsHotPointsPositions:(NSArray <NSString *>*)buttonsHotsPointsPositions forTags:(NSArray <NSNumber *>*)tags`

```sh
(GCCustomPOIInfo*)gcTourViewController:(id)tourViewController customPOIInfoForPOI:(GcPointOfInterestCustom*)customPOI
```
Méthode permettant de construire les custom POI. 
Pour construire un POI, on peut l’initialiser à partir d’une image locale ou d’une URL. 

Lors de son initialisation, on lui attribuera également sa taille grâce au constructeur : 
`- (instancetype)initWithImage:(UIImage *)poiImage andOverlay:(UIImage*)overlay withVisibility:(BOOL)visible andRound:(BOOL)rounded andPOISize:(CGSize)poiSize`

Lors du survol d’un POI, on peut également personnaliser son animation. Pour cela, on fera appel à cette méthode : 
`- (void)setAnimationType:(AnimationType)animationType withDuration:(CGFloat)animationDuration`
`BounceAndScale` et `FadeInAndOut` sont les deux types d’animations possibles.

Il est également possible d’afficher une Popup lors du survol d’un custom POI. 
Pour cela, on construira celle-ci à l’aide d’un XIB et on pourra l’initialiser comme ceci : 
`UIView *popup = [[[NSBundle mainBundle] loadNibNamed:@"PopupView" owner:self options:nil] objectAtIndex:0];`

Il suffira ensuite de faire appel à cette méthode pour l’afficher au-dessus du custom POI : 
`- (void)setPopupToDisplay:(UIView *)popupToDisplay withPopupSize:(CGSize)popupSize`

De même que les POI de type panorama, on peut ajouter des boutons au sein de la popup (cf ci-dessus).

```sh
(void)gcTourViewController:(id)tourViewController customPopupButtonTriggered:(NSInteger)popupButtonTag
```
Méthode appelée lorsqu'un bouton de la popup a été touché. Grâce au tag définit, on peut différencier quel bouton a été touché et ainsi définir une action particulière. 	

### Gestion de l'orientation 

Il est possible de définir les orientations souhaitées du contrôleur affichant le panorama et héritant de GcTourViewController. 
Pour cela, on utilisera la méthode `supportedInterfaceOrientations` pour définir les orientations désirées.
La méthode `shouldAutorotate` ne doit pas être définie à `false`, sinon l'auto-rotation sera bloquée.
Par défaut, toutes les orientations sont gérées.

###### Important : pour que le mode CardBoard soit correctement géré, il faut que le mode landscape soit supporté. Vous êtes responsables de l'orientation de cette vue.
### 
###### N.B : Si la classe fille est embarquée dans un navigation controller, c’est au navigation controller d’implémenter les supportedInterfaceOrientations, c’est lui qui est au-dessus de la pile.

### License
The ScenoVR SDK is free for software applications.
